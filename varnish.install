<?php
/**
 * @file
 * Install and update functions for the module.
 */

/**
 * Implements hook_requirements().
 *
 * Ensure that varnish's connection is good.
 */
function varnish_requirements($phase) {
  $requirements = array();
  // Ensure translations don't break during installation.
  $t = get_t();

  if ($phase == 'runtime') {
    $requirements['varnish']['title'] = $t('Varnish status');
    $status = varnish_get_status();
    if ($status === NULL) {
      $requirements['varnish']['value'] = $t('Required PHP extension not found. Install the <a href="https://php.net/manual/en/book.sockets.php">sockets</a> extension.');
      $requirements['varnish']['severity'] = REQUIREMENT_ERROR;
      return $requirements;
    }
    foreach ($status as $terminal => $state) {
      list($server, $port) = explode(':', $terminal);
      if (!$state) {
        $requirements['varnish']['value'] = $t('Varnish connection broken');
        $requirements['varnish']['severity'] = REQUIREMENT_ERROR;
        $requirements['varnish']['description'] = $t('The Varnish control terminal is not responding at %server on port %port.', array('%server' => $server, '%port' => $port));
        return $requirements;
      }
      else {
        $version = floatval(variable_get('varnish_version', 2.1));
        if ($version <= 2.1) {
          $requirements['varnish']['value'] = $t('Varnish is running. Observe more detailed statistics !link.', array('!link' => l($t('here'), 'admin/reports/varnish')));
        }
        else {
          $requirements['varnish']['value'] = $t('Running');
        }
      }
    }
  }
  return $requirements;
}

/**
 * Implements hook_uninstall().
 */
function varnish_uninstall() {
  // Delete variables.
  variable_del('varnish_bantype');
  variable_del('varnish_cache_clear');
  variable_del('varnish_cmdlength_limit');
  variable_del('varnish_control_key');
  variable_del('varnish_control_key_appendnewline');
  variable_del('varnish_control_terminal');
  variable_del('varnish_flush_cron');
  variable_del('varnish_front_domains');
  variable_del('varnish_socket_timeout');
  variable_del('varnish_version');
}
